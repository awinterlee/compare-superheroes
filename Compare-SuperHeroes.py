import time
import os
import winsound
import multiprocessing


heroes = {}

class Hero:
    def __init__(self, name, can_fly, has_super_strength, has_super_speed, is_invincible):
        self.name = name
        self.can_fly = can_fly
        self.has_super_strength = has_super_strength
        self.has_super_speed = has_super_speed
        self.is_invincible = is_invincible



def compare(hero1, hero2):

    hero1 = globals()[hero1]
    hero2 = globals()[hero2]
    print(f"Comparison between {hero1.name} and {hero2.name}:")
    print("+-------------------------+------------+------------+------------+------------+")
    print("| Attribute               |  Hero 1    |  Hero 2    |    Diff    |   Match    |")
    print("+-------------------------+------------+------------+------------+------------+")
    if hero1.can_fly != hero2.can_fly:
        diff = "✓" if hero1.can_fly else "✗"
        match = "✗" if hero1.can_fly else "✓"
        print(f"| Can fly                 | {hero1.can_fly:<10} | {hero2.can_fly:<10} | {diff:<10} | {match:<10} |")
    else:
        print(f"| Can fly                 | {hero1.can_fly:<10} | {hero2.can_fly:<10} | {'':<10} | ✓         |")
    if hero1.has_super_strength != hero2.has_super_strength:
        diff = "✓" if hero1.has_super_strength else "✗"
        match = "✗" if hero1.has_super_strength else "✓"
        print(f"| Has super strength      | {hero1.has_super_strength:<10} | {hero2.has_super_strength:<10} | {diff:<10} | {match:<10} |")
    else:
        print(f"| Has super strength      | {hero1.has_super_strength:<10} | {hero2.has_super_strength:<10} | {'':<10} | ✓         |")
    if hero1.has_super_speed != hero2.has_super_speed:
        diff = "✓" if hero1.has_super_speed else "✗"
        match = "✗" if hero1.has_super_speed else "✓"
        print(f"| Has super speed         | {hero1.has_super_speed:<10} | {hero2.has_super_speed:<10} | {diff:<10} | {match:<10} |")
    else:
        print(f"| Has super speed         | {hero1.has_super_speed:<10} | {hero2.has_super_speed:<10} | {'':<10} | ✓         |")
    if hero1.is_invincible != hero2.is_invincible:
        diff = "✓" if hero1.is_invincible else "✗"
        match = "✗" if hero1.is_invincible else "✓"
        print(f"| Is invincible           | {hero1.is_invincible:<10} | {hero2.is_invincible:<10} | {diff:<10} | {match:<10} |")
    else:
        print(f"| Is invincible           | {hero1.is_invincible:<10} | {hero2.is_invincible:<10} | {'':<10} | ✓         |")
    if hero1.name == hero2.name and hero1.can_fly == hero2.can_fly and hero1.has_super_strength == hero2.has_super_strength and hero1.has_super_speed == hero2.has_super_speed and hero1.is_invincible == hero2.is_invincible:
        print(f"|{'':<26}The two heroes are identical.{'':<27}|")
    print("+-------------------------+------------+------------+------------+------------+")

    # print(globals()[hero1].name + " "*20 + globals()[hero2]].name)
    # print(globals()[hero1]. + " "*20 + globals()[hero2]].name)
    # print(globals()[hero1].name + " "*20 + globals()[hero2]].name)
    # print(globals()[hero1].name + " "*20 + globals()[hero2]].name)
    # print(globals()[hero1].name + " "*20 + globals()[hero2]].name)
    # print(globals()[hero1].name + " "*20 + globals()[hero2]].name)

    # print(f"Comparison between {globals()[hero1].name} and {globals()[hero2].name}:")
    # if globals()[hero1].can_fly != globals()[hero2].can_fly:
    #     print(f"Can fly: {globals()[hero1].name} vs {globals()[hero2].name} cannot")
    # # if hero1.has_super_strength != hero2.has_super_strength:
    # #     print(f"Has super strength: {hero1.has_super_strength} vs {hero2.has_super_strength}")
    # # if hero1.has_super_speed != hero2.has_super_speed:
    # #     print(f"Has super speed: {hero1.has_super_speed} vs {hero2.has_super_speed}")
    # # if hero1.is_invincible != hero2.is_invincible:
    # #     print(f"Is invincible: {hero1.is_invincible} vs {hero2.is_invincible}")
    # # if hero1.name == hero2.name and hero1.can_fly == hero2.can_fly and hero1.has_super_strength == hero2.has_super_strength and hero1.has_super_speed == hero2.has_super_speed and hero1.is_invincible == hero2.is_invincible:
    # #     print("The two heroes are identical.")

def create_hero():
    current_hero_name = input("Tell me a Superhero's name! :")
    current_can_fly = input("Can they fly? ")

    if current_can_fly.lower() == "y" or current_can_fly.lower() == "yes":
        current_can_fly = True
    else:
        current_can_fly = False

    current_strength = input("Do they have super strength? ")

    if current_strength.lower() == "y" or current_strength.lower() == "yes":
        current_strength = True
    else:
        current_strength = False

    current_has_speed = input("Do they have super speed? ")

    if current_has_speed.lower() == "y" or current_has_speed.lower() == "yes":
        current_has_speed = True
    else:
        current_has_speed = False

    current_is_invincible = input("Are they invincible? ")

    if current_is_invincible.lower() == "y" or current_is_invincible.lower() == "yes":
        current_is_invincible = True
    else:
        current_is_invincible = False

    new_hero = Hero(current_hero_name, current_can_fly, current_strength, current_has_speed, current_is_invincible)
    print("Thanks! I'll add it to the list!")
    print("")
    time.sleep(1)

    # assign the new hero instance to a variable with the name of the hero
    globals()[current_hero_name] = new_hero
    hero_list.append(current_hero_name)
    return new_hero

hero_list = []
hero_name1 = ""
hero_name2 = ""

while True:
    print("Welcome to Hero Comparer v1.0! What would you like to do?")
    time.sleep(.5)
    print("You can:")
    time.sleep(.25)
    print("CREATE a hero.")
    time.sleep(.25)
    print("LIST the heroes you currently have.")
    time.sleep(.25)
    print("COMPARE heros.")
    time.sleep(.25)
    print("QUIT.")
    time.sleep(.5)
    print(" ")

    user_input = []

    current_input = input("): ").strip().lower()
    if not current_input:
        continue
    current_input = current_input.split()
    user_input.append(current_input)
    if current_input[0] == "done":
        break






    elif current_input[0] == "create":
        print("Okay, let's start a character!")
        print("")
        create_hero()
        print(globals()[hero_list[0]].name)


    elif current_input[0] == "compare":

        print("Let's compare these heroes!")
        print("type EXIT to return to the main menu!")
        first_hero = ["ok"]


        while first_hero[0] != "exit":
            first_hero = []



            first_hero = input("Which character do want to compare first? ")
            first_hero = (first_hero.lower()).split()
            matching_name1 = first_hero[0]


            if len(matching_name1) < 1:
                print("Please enter a valid hero name.")
            else:
                matching_name1 = next((name for name in hero_list if name.casefold() == first_hero[0].casefold()), None)
                if matching_name1:
                    hero_name1 = matching_name1
                    print("Great! I found " + matching_name1)
                else:
                    print("I don't see them in the list!")

            if hero_name1 and len(hero_name1) > 0:
                matching_name2 = input("Which character do you want to compare with " + hero_name1 + "? ")
                matching_name2 = matching_name2.lower().split()[0]
                matching_name2 = next((name for name in hero_list if name.casefold() == matching_name2.casefold()), None)
                if matching_name2:
                    hero_name2 = matching_name2
                    print("Great! Let's compare " + hero_name1 + " with " + hero_name2 + "!")
                    compare(hero_name1, hero_name2)
                    break
                else:
                    print("I don't see them in the list!")

            if first_hero == []:
                pass


    elif current_input[0] == "alex" and current_input[1] == "favorite" and current_input[2] == "thing":
        print("Are you sure?")
        time.sleep(2)
        for i in range(10, 0, -1):
            print(f"\r{i}", end='', flush=True)
            time.sleep(1)
        print("\rBlast off!")
        time.sleep(.25)
        print("⣿⣿⣿⣿⣿⣿⣿⣿⣿⠟⠛⢉⢉⠉⠉⠻⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⣿⣿⣿⠟⠠⡰⣕⣗⣷⣧⣀⣅⠘⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⣿⣿⠃⣠⣳⣟⣿⣿⣷⣿⡿⣜⠄⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⡿⠁⠄⣳⢷⣿⣿⣿⣿⡿⣝⠖⠄⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⠃⠄⢢⡹⣿⢷⣯⢿⢷⡫⣗⠍⢰⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⡏⢀⢄⠤⣁⠋⠿⣗⣟⡯⡏⢎⠁⢸⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⠄⢔⢕⣯⣿⣿⡲⡤⡄⡤⠄⡀⢠⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⠇⠠⡳⣯⣿⣿⣾⢵⣫⢎⢎⠆⢀⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⠄⢨⣫⣿⣿⡿⣿⣻⢎⡗⡕⡅⢸⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⠄⢜⢾⣾⣿⣿⣟⣗⢯⡪⡳⡀⢸⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⠄⢸⢽⣿⣷⣿⣻⡮⡧⡳⡱⡁⢸⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⡄⢨⣻⣽⣿⣟⣿⣞⣗⡽⡸⡐⢸⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⡇⢀⢗⣿⣿⣿⣿⡿⣞⡵⡣⣊⢸⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⡀⡣⣗⣿⣿⣿⣿⣯⡯⡺⣼⠎⣿⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣧⠐⡵⣻⣟⣯⣿⣷⣟⣝⢞⡿⢹⣿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⡆⢘⡺⣽⢿⣻⣿⣗⡷⣹⢩⢃⢿⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⣷⠄⠪⣯⣟⣿⢯⣿⣻⣜⢎⢆⠜⣿⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⣿⣿⣿⡆⠄⢣⣻⣽⣿⣿⣟⣾⡮⡺⡸⠸⣿⣿⣿⣿")
        time.sleep(.25)
        print("⣿⣿⡿⠛⠉⠁⠄⢕⡳⣽⡾⣿⢽⣯⡿⣮⢚⣅⠹⣿⣿⣿")
        time.sleep(.25)
        print("⡿⠋⠄⠄⠄⠄⢀⠒⠝⣞⢿⡿⣿⣽⢿⡽⣧⣳⡅⠌⠻⣿")
        time.sleep(.25)
        print("⠁⠄⠄⠄⠄⠄⠐⡐⠱⡱⣻⡻⣝⣮⣟⣿⣻⣟⣻⡺⣊⣿")
        time.sleep(10)
    elif current_input[0] == "list":

        print("Alright, here's a list of your current characters!")
        print("")
        time.sleep(.5)
        if len(hero_list) == 0:
            print("No heroes yet! Try adding some!")
            print("")
        for characters in hero_list:
            print(characters)
            print("")
        time.sleep(.5)

    elif current_input[0] == "music":

        if os.name == 'nt':
            sound_file = "SystemExit"
            total_duration = 120
            num_sounds = 10
            delay = 0.5
            start_time = time.time()

            while time.time() - start_time < total_duration:
                for i in range(num_sounds):
                    winsound.PlaySound(sound_file, winsound.SND_ASYNC)
                time.sleep(delay)

                # Play an MP3 file on macOS or Linux
        elif os.name == 'posix':  # for macOS or Linux
            sound_file = "/System/Library/Sounds/Glass.aiff"
            total_duration = 120
            num_sounds = 5
            delay = 0.5
            start_time = time.time()
            while time.time() - start_time < total_duration:
                for i in range(num_sounds):
                    os.system('afplay {} &'.format(sound_file))
                time.sleep(delay)

    else:
        print("That's not a valid entry. Try again, citizen!")
        print("")



hero_list.append(current_hero.name)

print(hero_list)
